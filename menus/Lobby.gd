extends Control

func _ready():
	Orchestrator.current_level = self

func _process(_delta):
	update_players_list()

func update_players_list():
	for c in $Background/VBoxContainer/PlayersPanel/List.get_children():
		$Background/VBoxContainer/PlayersPanel/List.remove_child(c)
	for pid in Network.players.keys():
		var player: NetworkPlayer = Network.players.get_player(pid)
		var label: Label = Label.new()
		label.text = player.get_name()
		if player.ready():
			label.add_color_override("font_color", Color.green)
		else:
			label.add_color_override("font_color", Color.red)
		$Background/VBoxContainer/PlayersPanel/List.add_child(label)

func _on_Ready_toggled(button_pressed: bool):
	Network.player_ready(button_pressed)
