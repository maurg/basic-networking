extends Control

const LOBBY_PATH = "res://menus/Lobby.tscn"

const NAMES = [
	"Final Master",
	"Zero Predator",
	"Besiege of Rule",
	"Revolt of Aggression",
	"Dynasty and Treasures",
	"Valor and Fire",
	"Firemind",
	"Alphafire",
	"Datafight",
	"Cloudrain"
]

func _ready():
	Orchestrator.current_level = self
	var random: RandomNumberGenerator = RandomNumberGenerator.new()
	random.randomize()
	var name_select: int = random.randi_range(0, NAMES.size() - 1)
	$Background/Menu/PlayerName.text = NAMES[name_select]

func _process(_delta):
	if Network.connected():
		Network.try_register($Background/Menu/PlayerName.text)
	elif Network.registered():
# warning-ignore:return_value_discarded
		get_tree().change_scene(LOBBY_PATH)

func _on_ConnectNew_pressed():
	var ip: String = $Background/Menu/ConnectNew/IP.text
	join_server(ip)

func _on_ConnectLast_pressed():
	#var ip: String = $Background/Menu/ConnectLast/IP.text
	join_server("127.0.0.1")

func valid_ip(ip: String) -> bool:
	var regex: RegEx = RegEx.new()
# warning-ignore:return_value_discarded
	regex.compile("\\A\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\z")
	var result: RegExMatch = regex.search(ip)
	if result:
		return true
	$Background/Menu/ErrorMessage.text = "Invalid IP address"
	return false

func join_server(ip: String):
	if valid_ip(ip):
		var error: int = Network.connect_to_server(ip)
		if error != OK:
			$Background/Menu/ErrorMessage.text = "Connection Error: " + str(error)
	else:
		$Background/Menu/ErrorMessage.text = "Invalid ip address"

func _on_StartServer_pressed():
	var error: int = Network.create_server()
	if error != OK:
		$Background/Menu/ErrorMessage.text = "Connection Error: " + str(error)
	else:
		Network.register_player(Config.SERVER_ID, $Background/Menu/PlayerName.text)
