extends Control

func set_max_value(value: float) -> void:
	$TextureProgress.max_value = value
	
func set_value(value: float) -> void:
	$TextureProgress.value = value
