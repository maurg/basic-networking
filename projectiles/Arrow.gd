extends KinematicBody2D

export(float) var speed = 200.0
export(float) var power = 10.0
var velocity: Vector2 = Vector2()
var direction: Vector2
var target: Node

func set_target(value: Node) -> void:
	target = value
	direction = position.direction_to(target.position)
	rotation = atan2(direction.y, direction.x)

func _physics_process(delta: float) -> void:
	var collision: KinematicCollision2D = move_and_collide(direction * speed * delta)
	if collision != null:
		collision.collider.attacked(power)
		queue_free()
