extends Node

var players: NetworkPlayers = NetworkPlayers.new()
var network_id = -1

var send_buffer: Buffer = Buffer.new(Config.SEND_BUFFER_SIZE)
var jitter_buffer: JitterBuffer = JitterBuffer.new(Config.JITTER_BUFFER_SIZE)

func _ready():
	pause_mode = PAUSE_MODE_PROCESS
# warning-ignore:return_value_discarded
	get_tree().connect("connected_to_server", self, "_connected_to_server")
# warning-ignore:return_value_discarded
	get_tree().connect("server_disconnected", self, "_server_disconnected")
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_connected", self, "_player_connected")

func _player_connected(pid: int):
	var player: NetworkPlayer = players.get_or_new_player(pid)
	player.set_connection_state(NetworkPlayer.CONNECTED)

	if is_server():
		rpc_id(pid, "update_players_list", players.serialize())

func _player_disconnected(id: int):
	if is_server():
		rpc("update_connection_state", id, NetworkPlayer.DISCONNECTED)

func _connected_to_server():
	pass

func _server_disconnected():
	update_connection_state(network_id, NetworkPlayer.DISCONNECTED)

remotesync func update_connection_state(pid: int, state: int):
	players.get_player(pid).set_connection_state(state)

remote func update_players_list(remote_players: Dictionary):
	players = NetworkPlayers.new(remote_players)

func create_server() -> int:
	var peer = NetworkedMultiplayerENet.new()
	var error: int = peer.create_server(Config.GAME_PORT, Config.MAX_PLAYERS)
	get_tree().set_network_peer(peer)
	network_id = peer.get_unique_id()
	return error

func connect_to_server(ip: String) -> int:
	var peer: NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
	var error: int = peer.create_client(ip, Config.GAME_PORT)
	if error == OK:
		get_tree().set_network_peer(peer)
		network_id = peer.get_unique_id()
		players.set_player(network_id,  NetworkPlayer.new())
	return error

func try_register(player_name: String) -> void:
	var self_player: NetworkPlayer =  players.get_player(network_id)
	if !self_player.registering():
		self_player.set_connection_state(NetworkPlayer.REGISTERING)
		rpc_id(1, "register_player", network_id, player_name)

remote func register_player(pid: int, player_name: String) -> void:
	var info: NetworkPlayer = NetworkPlayer.new()
	info.set_name(player_name)
	info.set_connection_state(NetworkPlayer.REGISTERED)
	rpc("add_player", pid, info.serialize())

remotesync func add_player(pid: int, info: Dictionary) -> void:
	players.set_player(pid, NetworkPlayer.new(info))

func network_ready() -> bool:
	return get_tree().has_network_peer()

func registered() -> bool:
	return players.get_player(network_id).registered() if network_ready() else false

func connected() -> bool:
	return players.get_player(network_id).connected() if network_ready() else false

func start_game():
	pause_mode = PAUSE_MODE_PROCESS

func continue_game():
	pause_mode = PAUSE_MODE_PROCESS

func pause_game():
	pause_mode = PAUSE_MODE_STOP

func stop_game():
	pause_game()

func players_ready() -> bool:
	for player in Network.players.get_all():
		if !player.ready():
			return false
	return true

func players_starting() -> bool:
	for player in Network.players.get_all():
		if !player.starting():
			return false
	return true

func game_state() -> int:
	return players.get_player(network_id).get_game_state()

func player_ready(ready: bool):
	var new_state: int = NetworkPlayer.READY if ready else NetworkPlayer.WAITING
	rpc("update_game_state", network_id, new_state)

func game_starting():
	#consider syncing clocks at this stage
	rpc("update_game_state", network_id, NetworkPlayer.STARTING)

func player_game_starting(cid):
	rpc("update_game_state", cid, NetworkPlayer.STARTING)

func game_playing():
	rpc("update_game_state", network_id, NetworkPlayer.PLAYING)

func game_ready():
	rpc("update_game_state", network_id, NetworkPlayer.READY)

func game_paused():
	rpc("update_game_state", network_id, NetworkPlayer.PAUSED)

remotesync func update_game_state(pid: int, state: int):
	players.get_player(pid).set_game_state(state)

func is_game_starting() -> bool:
	return players.get_player(network_id).starting()

func is_game_ready() -> bool:
	return players.get_player(network_id).ready()

func is_game_playing() -> bool:
	return players.get_player(network_id).playing()

func is_game_waiting() -> bool:
	return players.get_player(network_id).waiting()

func is_game_paused() -> bool:
	return players.get_player(network_id).waiting()

func is_server() -> bool:
	return get_tree().has_network_peer() && get_tree().is_network_server()

# Clients send ownable objects state to the server. Server sends all objects states to clients
# This implementation has a delay from peer to server of at least 1 Jitter Buffer Size and from peer
# to peer of 2 Jitter Buffers size.
func send_ownable_state(frame_number: int, state: Dictionary):
	if is_server():
		send_buffer.add(frame_number, state)
		rpc_unreliable("client_receive_ownable_states", send_buffer.serialize())
	else:
		send_buffer.add(frame_number, state)
		rpc_unreliable_id(Config.SERVER_ID, "server_receive_ownable_states", network_id, send_buffer.serialize())

# Server receive state update from clients add to individual JitterBuffer's per player
remote func server_receive_ownable_states(pid: int, buffer: Dictionary):
	players.get_player(pid).jitter_buffer.add_from_dict(buffer)

# Received by clients with world update from server and add to jitter buffer
remotesync func client_receive_ownable_states(receive_buffer: Dictionary):
	jitter_buffer.add_from_dict(receive_buffer)

# As a client get the last update form the server
func client_get_objects_update() -> Dictionary:
		return jitter_buffer.get_entry()

# As a server retrieve the oldest valid entry from every players JitterBuffer
func server_get_players_objects_state() -> Dictionary:
	var players_entries: Dictionary = {}
	for pid in players.keys():
		var player_buffer: JitterBuffer = players.get_player(pid).jitter_buffer
		if player_buffer.has_entry():
			players_entries[pid] = player_buffer.get_entry()
	return players_entries
