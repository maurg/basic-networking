extends Node

const FLOAT_EPSILON = 0.00001

static func compare_floats(a: float, b: float, epsilon: float = FLOAT_EPSILON) -> bool:
	return abs(a - b) <= epsilon

static func compare_vector2(a: Vector2, b: Vector2) -> bool:
	return compare_floats(a.x, b.x) && compare_floats(a.y, b.y)

static func compare_dictionaries(a: Dictionary, b: Dictionary) -> bool:
	if a.hash() == b.hash():
		return true

	if a.size() == b.size() && a.keys() == b.keys():
		for key in a.keys():
			if typeof(a[key]) == TYPE_DICTIONARY:
				if !compare_dictionaries(a[key], b[key]):
					return false
			else:
				if a[key] != b[key]:
					return false
	return true

static func merge_dicts(dict1, dict2) -> Dictionary:
	if dict1 == null:
		dict1 = dict2
	elif dict2 != null:
		for key in dict2.keys():
			dict1[key] = dict2[key]
	return dict1
