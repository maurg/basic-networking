class_name NetworkPlayers

var players: Dictionary = {}

func _init(serialized_players: Dictionary = {}):
	for id in serialized_players.keys():
		players[id] = NetworkPlayer.new(serialized_players[id])

func serialize() -> Dictionary:
	var res: Dictionary = {}
	for id in players.keys():
		res[id] = players[id].serialize()
	return res

func get_all() -> Array:
	return players.values()

func get_player(id: int) -> NetworkPlayer:
	return players[id]

func get_or_new_player(id: int) -> NetworkPlayer:
	if !players.has(id):
		set_player(id, NetworkPlayer.new())
	return players[id]

func set_player(id: int, player: NetworkPlayer):
	players[id] = player

func keys() -> Array:
	return players.keys()
