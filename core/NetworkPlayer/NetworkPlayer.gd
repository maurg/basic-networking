class_name NetworkPlayer

enum {NAME, CONNECTION_STATE, GAME_STATE}
enum {CONNECTED, REGISTERING, REGISTERED, DISCONNECTED}
enum {WAITING, READY, STARTING, PLAYING, PAUSED, ENDING}

var player: Dictionary = {}
var jitter_buffer: JitterBuffer = JitterBuffer.new(Config.JITTER_BUFFER_SIZE)

func _init(value: Dictionary = {}):
	player = value
	if !player.has(GAME_STATE):
		player[GAME_STATE] = WAITING
	if !player.has(CONNECTION_STATE):
		player[CONNECTION_STATE] = DISCONNECTED
	if !player.has(NAME):
		player[NAME] = ""

func serialize() -> Dictionary:
	return player

func set_game_state(value: int):
	player[GAME_STATE] = value

func get_game_state() -> int:
	return player[GAME_STATE]

func set_connection_state(value: int):
	player[CONNECTION_STATE] = value

func get_connection_state() -> int:
	return player[CONNECTION_STATE]

func set_name(value: String):
	player[NAME] = value

func get_name() -> String:
	return player[NAME]

func connected() -> bool:
	return get_connection_state() == CONNECTED

func registering() -> bool:
	return get_connection_state() == REGISTERING

func registered() -> bool:
	return get_connection_state() == REGISTERED

func ready() -> bool:
	return get_game_state() == READY

func starting() -> bool:
	return get_game_state() == STARTING

func waiting() -> bool:
	return get_game_state() == WAITING

func playing() -> bool:
	return get_game_state() == PLAYING
