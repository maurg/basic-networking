extends Node

const GAME_PORT = 17893
const MAX_PLAYERS = 4
const SERVER_ID = 1
const UPDATE_SEND_DIVIDER = 6
const SEND_BUFFER_SIZE = 8
const RECEIVE_BUFFER_SIZE = 32
const JITTER_BUFFER_SIZE = 6

# in milliseconds
const GAME_COUNTDOWN_TIME = 3000
