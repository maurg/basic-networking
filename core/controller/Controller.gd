extends Node

const up: String = 'ui_up'
const down: String = 'ui_down'
const right: String = 'ui_right'
const left: String = 'ui_left'
const action_1: String = 'ui_action1'
const action_2: String = 'ui_action2'

var velocity: Vector2 = Vector2()
var action1_pressed: bool = false
var action2_pressed: bool = false

func _physics_process(_delta):
	velocity = Vector2()
	action1_pressed = false
	action2_pressed = false

	if Input.is_action_pressed(up):
		velocity.y -= 1
	if Input.is_action_pressed(down):
		velocity.y += 1
	if Input.is_action_pressed(right):
		velocity.x += 1
	if Input.is_action_pressed(left):
		velocity.x -= 1
	if Input.is_action_just_pressed(action_1):
		action1_pressed = true
	if Input.is_action_just_pressed(action_2):
		action2_pressed = true

	var player: Player = get_parent()
	player.velocity = velocity
	player.action1 = action1_pressed
	player.action2 = action2_pressed
