extends Node

var current_level: Node = null
var players: Dictionary = {}
var player_scene: PackedScene = preload("res://characters/players/Player.tscn")
var controller_scene: PackedScene = preload("res://core/controller/Controller.tscn")
var ownable_objects: Dictionary = {}

var game_start_time: int

enum { LIFE_STATE, STATE }
enum { LIVE, DEAD }

func _ready():
	pause_mode = PAUSE_MODE_PROCESS

# The syncing of ownable objects assumes that no ownable object is created after the level starts
func _physics_process(_delta):
	if !Network.network_ready():
		return

	if Network.is_server():
		if Network.is_game_ready() && Network.players_ready():
			rpc("prepare_game")

	if Network.is_game_starting() && Network.players_starting() && OS.get_system_time_msecs() >= game_start_time:
		start_game()

	if Network.is_game_playing():
		if Network.is_server():
			var players_objects: Dictionary = Network.server_get_players_objects_state()
			for pid in players_objects.keys():
				set_objects_state(players_objects[pid])
		else:
			set_objects_state(Network.client_get_objects_update())

		if !Network.is_game_paused():
			var local_objects: Dictionary = get_objects_state(Network.is_server())
			Network.send_ownable_state(current_level.current_frame, local_objects)

remotesync func prepare_game():
	change_level("res://BasicMap.tscn")
	get_tree().paused = true

	if Network.is_server():
		game_start_time = OS.get_system_time_msecs() + Config.GAME_COUNTDOWN_TIME
		Network.game_starting()
	else:
		rpc_id(Config.SERVER_ID, "sync_start", Network.network_id, OS.get_system_time_msecs())

remote func sync_start(cid: int , remote_time: int) -> void:
	var remote_start: int = remote_time + game_start_time - OS.get_system_time_msecs()
	rpc_id(cid, "receive_start_time", remote_start)

remote func receive_start_time(start_time: int) -> void:
	game_start_time = start_time
	rpc_id(Config.SERVER_ID, "ack_start", Network.network_id)

remote func ack_start(cid: int) -> void:
	Network.player_game_starting(cid)

remotesync func pause():
	get_tree().paused = true
	Network.game_paused()

func start_game():
	get_tree().paused = false
	Network.game_playing()

func change_level(path: String):
	var root: Viewport = get_tree().get_root()
	root.remove_child(current_level)
	current_level.queue_free()

	var level: Node = load(path).instance()
	root.add_child(level)
	current_level = level

	for pid in Network.players.keys():
		var player: Player = create_player(pid)
		players[pid] = player
		level.add_player(player)
		if pid == Network.network_id:
			player.add_child(controller_scene.instance())
			player.get_node("AuthorityHandler").request_ownership()

func create_player(pid: int) -> Player:
	var player: Player = player_scene.instance()
	player.name = str(pid)
	return player

func get_objects_state(full: bool = false) -> Dictionary:
	var ownable_state: Dictionary = {}
	for object_path in ownable_objects.keys():
		if ownable_objects[object_path] == LIVE:
			ownable_state[object_path] = {
					STATE: get_node(NodePath(object_path)).serialize(full),
					LIFE_STATE: LIVE
				}
		elif Network.is_server():
			ownable_state[object_path] = { LIFE_STATE: DEAD }
	return ownable_state

func set_or_create_object(path: String, state: Dictionary):
	var node_path: NodePath = NodePath(path)
	var node: Node = get_node_or_null(node_path)
	var dead: bool = state[LIFE_STATE] == DEAD
	if node == null && !ownable_objects.has(path) && !dead:
		node = AuthorityHandler.new().build(state[STATE], self)
	if node != null:
		if dead:
			node.destroy()
		else:
			node.update_state(state[STATE])

func set_objects_state(objects: Dictionary):
	for node_path in objects.keys():
		set_or_create_object(node_path, objects[node_path])

func track_object(object: Node):
	var object_path: String = object.get_path()
	if ownable_objects.has(object_path):
		push_error("trying to add an already existing object")
		return
	ownable_objects[object_path] = LIVE

func remove_object(object: Node):
	var object_path: String = object.get_path()
	if !ownable_objects.has(object_path):
		push_error("trying to remove a non-existing object")
		return
	ownable_objects[object_path] = DEAD
