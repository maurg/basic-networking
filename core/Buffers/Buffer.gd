class_name Buffer

var buffer: Dictionary = {}
var limit = 0

func _init(_limit: int = 8, _buffer: Dictionary = {}):
	limit = _limit
	buffer = _buffer

func add(key: int, value):
	buffer[key] = value
	while buffer.size() >= limit:
# warning-ignore:return_value_discarded
		buffer.erase(buffer.keys().min())

func retrieve(key: int):
	return buffer[key]

func erase(key: int):
	if buffer.has(key):
		var erased = buffer[key]
# warning-ignore:return_value_discarded
		buffer.erase(key)
		return erased
	return null

func serialize():
	return buffer

func keys():
	return buffer.keys()

func merge(other_buffer: Buffer):
	for key in other_buffer.keys():
		set(key,  other_buffer.get(key))

func empty():
	return buffer.empty()

func clear():
	buffer = {}
