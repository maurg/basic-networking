extends Buffer

class_name JitterBuffer

var last_retrieved: int = -1
var jitter_size: int

func _init(_jitter_size: int = 6, _limit: int = 60):
	jitter_size = _jitter_size
	._init(_limit)

func add_from_buffer(buffer: Buffer):
	for key in buffer.keys():
		var entry = buffer.retrieve(key)
		add_entry(key, entry)

func add_from_dict(dict: Dictionary):
	for key in dict.keys():
		var entry = dict[key]
		add_entry(key, entry)

func add_entry(process_num: int, entry):
	if process_num <= last_retrieved:
		return
	add(process_num, entry)

func has_entry() -> bool:
	return buffer.size() >= jitter_size

# Add logic to skip updates that are too old, and reduce the jitter buffer to its size
func get_entry():
	if has_entry():
		var oldest_entry_id: int = buffer.keys().min()
		last_retrieved = oldest_entry_id
		return erase(oldest_entry_id)
	return {}
