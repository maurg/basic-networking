extends Node2D

class_name AuthorityHandler

var ownership_sequence: int = 0
var ownership_id: int = NO_OWNER

var authority_sequence: int = 0
var authority_id: int = NO_OWNER

var update_sequence: int = 0
var authority_timeout_counter: int = 0

var parent: Node
var parent_filename: String
var parent_path: String

const NO_OWNER: int = -1
const AUTHORITY_TIMEOUT = 10
enum { OWNER, OWNERSHIP_SEQUENCE, AUTHORITY, AUTHORITY_SEQUENCE, UPDATE_SEQUENCE,
	PARENT, PARENT_SCENE, PARENT_PATH }

func _ready():
	parent = get_parent()
	Orchestrator.track_object(self)

func _exit_tree():
	Orchestrator.remove_object(self)

func _physics_process(_delta: float):
	if authority_id == Network.network_id:
		if authority_timeout_counter <= AUTHORITY_TIMEOUT:
			authority_timeout_counter += 1
		else:
			pass
			#relinquish_authority()

func request_authority():
	authority_id = Network.network_id
	authority_sequence += 1
	authority_timeout_counter = 0

func relinquish_authority():
	if authority_id == Network.network_id:
		authority_id = NO_OWNER
		authority_sequence += 1

func request_ownership():
	if ownership_id == NO_OWNER && ownership_id != Network.network_id:
		ownership_sequence += 1
		ownership_id = Network.network_id
		update_sequence = 0

func relinquish_ownership():
	if ownership_id == Network.network_id:
		ownership_id = NO_OWNER
		ownership_sequence += 1
		update_sequence = 0

func validate_ownership(sequence: int) -> bool:
	return ownership_id == NO_OWNER && sequence == ownership_sequence + 1

func serialize(full_info: bool) -> Dictionary:
	var result: Dictionary = {
		OWNER: ownership_id,
		OWNERSHIP_SEQUENCE: ownership_sequence,
		AUTHORITY: authority_id,
		AUTHORITY_SEQUENCE: authority_sequence
	}

	if is_owner() || (has_no_owner() && full_info):
		update_sequence += 1
		result[UPDATE_SEQUENCE] = update_sequence
		result[PARENT] = parent.serialize()
		result[PARENT_SCENE] = get_parent_filename()
		result[PARENT_PATH] = get_parent_path()
	elif full_info:
		result[UPDATE_SEQUENCE] = update_sequence
		result[PARENT] = parent.serialize()
		result[PARENT_SCENE] = get_parent_filename()
		result[PARENT_PATH] = get_parent_path()

	return result

func get_parent_filename() -> String:
	if !parent_filename:
		parent_filename = parent.filename
	return parent_filename

func get_parent_path() -> String:
	if !parent_path:
		parent_path = parent.get_path()
	return parent_path

func has_no_owner():
	return ownership_id == NO_OWNER && authority_id == NO_OWNER

func is_owner():
	return ownership_id == Network.network_id || (ownership_id == NO_OWNER && authority_id == Network.network_id)

func update_state(state: Dictionary):
	if state[OWNER] == Network.network_id || (state[OWNER] == NO_OWNER && state[AUTHORITY] == Network.network_id):
		return

	if Network.is_server():
		if state[OWNER] != ownership_id && state[OWNERSHIP_SEQUENCE] <= ownership_sequence:
			return
		if state[OWNER] == NO_OWNER && state[AUTHORITY] != authority_id && state[AUTHORITY_SEQUENCE] <= authority_sequence:
			return
	else:
		if ownership_id == Network.network_id && state[OWNER] != ownership_id && ownership_sequence > state[OWNERSHIP_SEQUENCE]:
			return
		if state[OWNER] == NO_OWNER && authority_id == Network.network_id && state[AUTHORITY] != authority_id && authority_sequence > state[AUTHORITY_SEQUENCE]:
			return

	ownership_id = state[OWNER]
	ownership_sequence = state[OWNERSHIP_SEQUENCE]
	authority_id = state[AUTHORITY]
	authority_sequence = state[AUTHORITY_SEQUENCE]

	if state.has(UPDATE_SEQUENCE) && state[UPDATE_SEQUENCE] > update_sequence:
		update_sequence = state[UPDATE_SEQUENCE]
		parent.update_state(state[PARENT])

func build(data: Dictionary, in_tree_node: Node) -> Node:
	var node: Node = load(data[PARENT_SCENE]).instance()
	node.set_name(get_name_from_path(data[PARENT_PATH]))
	var location: Node = in_tree_node.get_node(get_parent_path_from_path(data[PARENT_PATH]))
	location.add_child(node)
	return node.get_node("AuthorityHandler")

func get_name_from_path(path: String) -> String:
	var pos: int = path.find_last('/')
	if pos == -1:
		return path
	return path.right(pos)

func get_parent_path_from_path(path: String) -> String:
	var pos: int = path.find_last('/')
	if pos == -1:
		return path
	return path.left(pos + 1)

func destroy():
	parent.queue_free()
