extends Node2D

class_name Spawner

export (PackedScene) var spawn_scene: PackedScene
export (float) var spawn_interval: float = 5.0
export (bool) var timed_spawn: bool = true
export (int) var max_spawns: int = 10
export (Vector2) var spawned_target: Vector2

var sequence: int = 0
var spawn_timer: Timer
var total_spawned: int = 0

func _ready():
	if !Network.is_server():
		return
	if timed_spawn:
		spawn_timer = Timer.new()
		spawn_timer.wait_time = spawn_interval
		spawn_timer.one_shot = true
	# warning-ignore:return_value_discarded
		spawn_timer.connect("timeout",self,"_on_spawn_timer_timeout")
		add_child(spawn_timer)
		spawn_timer.start()

func spawn() -> Node2D:
	if Network.is_server() && total_spawned < max_spawns:
		var spawned = spawn_scene.instance()
		spawned.position_target = spawned_target
		spawned.name = generate_spawned_name()
		get_level().add_minion(spawned, position)
		total_spawned += 1
		return spawned
	else:
		return null

func get_level() -> Node:
	return get_parent()

func generate_spawned_name() -> String:
	sequence += 1
	return get_name() + '_Spawned_' + str(sequence)

func _on_spawn_timer_timeout():
# warning-ignore:return_value_discarded
	spawn()
	spawn_timer.start()
