extends Node

var player_count: int = 0
var ready: bool = false
var current_frame: int = 0

func _ready():
	ready = true
	current_frame = 0

func _physics_process(_delta: float):
	current_frame += 1

func add_player(player: Player):
	var pos: Position2D = $Spawn.get_child(player_count)
	player.position = pos.position
	$Players.add_child(player)

	player_count += 1

func add_minion(minion: Minion, position: Vector2):
	minion.position = position
	$Minions.add_child(minion)
