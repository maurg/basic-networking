extends RigidBody2D

enum { POSITION, ROTATION, ANG_VEL, LIN_VEL, SLEEP }

func _ready():
# warning-ignore:return_value_discarded
	connect("body_entered", self, "_contact_with_body")

func serialize() -> Dictionary:
	return {
			SLEEP: sleeping,
			POSITION: position,
			ROTATION: rotation,
			ANG_VEL: angular_velocity,
			LIN_VEL: linear_velocity
		}

func update_state(state: Dictionary) -> void:
	position = state[POSITION]
	rotation = state[ROTATION]
	#angular_velocity = state[ANG_VEL]
	#linear_velocity = state[LIN_VEL]

func _contact_with_body(body: Node):
	if body.has_method("interact_with_rigidbody"):
		body.interact_with_rigidbody(self)

func request_authority():
	$AuthorityHandler.request_authority()

