extends Minion

class_name MeleeMinion

export(AudioStreamOGGVorbis) var audio_stream

func _ready() -> void:
	audio_stream.loop = false
	$AudioStreamPlayer2D.stream = audio_stream

func get_attack_detector() -> Node:
	return $RayCast2D

func get_target() -> Node:
	if $RayCast2D.is_colliding():
		return $RayCast2D.get_collider()
	return null

func play_attack_sound() -> void:
	$AudioStreamPlayer2D.play()
