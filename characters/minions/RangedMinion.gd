extends Minion

class_name RangedMinion

export(int) var initial_ammo = 2
export(PackedScene) var projectile_scene
export(AudioStreamOGGVorbis) var audio_stream
var attack_range: float
var targets: Dictionary = {}
var current_target: Node = null
var arrow_sprite_scene: PackedScene = preload("res://projectiles/ArrowSprite.tscn")

func _ready() -> void:
	attack_range = $RangeArea2D/CollisionShape2D.shape.radius
	audio_stream.loop = false
	$AudioStreamPlayer2D.stream = audio_stream
	set_ammo(initial_ammo)

func _physics_process(_delta: float) -> void:
	if current_target == null:
		$Label.text = ""
	else:
		$Label.text = current_target.get_name()

	for i in $AmmoBox.get_child_count():
		$AmmoBox.get_child(i).queue_free()
	for i in range(ammo):
		var sprite: Sprite = arrow_sprite_scene.instance()
		sprite.rotation_degrees = -45
		sprite.position = Vector2(-20, 10) + (Vector2(-2, 0) * i)
		$AmmoBox.add_child(sprite)

func can_attack():
	return .can_attack() && ammo > 0

func get_attack_detector() -> Node:
	return $RangeArea2D

func get_target() -> Node:
	if current_target != null && is_instance_valid(current_target):
		return current_target

	current_target = get_next_target()
	return current_target

# Currently getting a random target
func get_next_target() -> Node:
	for target in targets.keys():
		if is_instance_valid(target):
			return target
		else:
# warning-ignore:return_value_discarded
			targets.erase(target)
	return null

func attack(target: Node) -> void:
	set_ammo(ammo - 1)
	var projectile: Node = projectile_scene.instance()
	projectile.global_position = $Sprite/Weapon.global_position
	projectile.set_target(target)
	projectile.power = power
	projectile.collision_mask = $RangeArea2D.collision_mask
	Orchestrator.current_level.add_child(projectile)

func play_attack_sound() -> void:
	$AudioStreamPlayer2D.play()

func _on_RangeArea2D_body_entered(body):
	targets[body] = null

func _on_RangeArea2D_body_exited(body):
# warning-ignore:return_value_discarded
	targets.erase(body)
	if body == current_target:
		current_target = null
