extends KinematicBody2D

class_name Minion

var attack_timer: Timer
var attack_enabled: bool = true
var health: float
var dead: bool = false
const ALLY: String = "Ally"
const ENEMY: String = "Enemy"

export(String, "Ally", "Enemy") var team = ALLY
export (bool) var interactable = false
export(float) var power = 10.0
export(float) var initial_health = 100.0
export(float) var attack_delay = 2.0
export (int) var speed = 100
export (Vector2) var position_target
export (bool) var movable = false
export (int) var frame = 151

var interact_type: int = InteractType.OTHER
var velocity: Vector2 = Vector2()
var ammo: int = 0 setget set_ammo

enum { HEALTH, POSITION, AMMO }

func _ready() -> void:
	attack_timer = Timer.new()
	attack_timer.wait_time = attack_delay
	attack_timer.one_shot = true
# warning-ignore:return_value_discarded
	attack_timer.connect("timeout",self,"_on_attack_timer_timeout")
	add_child(attack_timer)
	health = initial_health
	$HealthBar.set_max_value(initial_health)
	$HealthBar.set_value(health)
	$Sprite.frame = frame
	if team == ALLY:
		set_collision_layer_bit(Layers.ALLY_BODY, true)
		set_collision_layer_bit(Layers.INTERACTABLE, true)
		set_collision_mask_bit(Layers.ENEMY_BODY, true)
		set_collision_mask_bit(Layers.PLAYER_BODY, true)
		get_attack_detector().set_collision_mask_bit(Layers.ENEMY_BODY, true)
	elif team == ENEMY:
		set_collision_layer_bit(Layers.ENEMY_BODY, true)
		set_collision_mask_bit(Layers.ALLY_BODY, true)
		set_collision_mask_bit(Layers.PLAYER_BODY, true)
		get_attack_detector().set_collision_mask_bit(Layers.ALLY_BODY, true)

func _physics_process(delta) -> void:
	if dead:
		return

	if !dead && health <= 0.0:
		start_death()

	var target: Object = get_target()
	if target != null:
		try_attack(target)
	try_move(delta)

	$HealthBar.set_value(health)

func set_ammo(value: int):
	ammo = value

func change_ammo(value: int) -> void:
	$AuthorityHandler.request_authority()
	set_ammo(ammo + value)

func try_move(_delta: float) -> void:
	if can_move():
		velocity = position.direction_to(position_target)
		if velocity.x != 0:
			$Sprite.flip_h = velocity.x < 0
		if velocity != Vector2(0,0):
			$RayCast2D.rotation = atan2(velocity.y, velocity.x)

		if position.distance_to(position_target) > 5:
			velocity = move_and_slide(velocity * speed)

func can_move() -> bool:
	return movable

func can_attack() -> bool:
	return attack_enabled

func try_attack(target: Node) -> void:
	if can_attack():
		attack(target)
		play_attack_sound()
		play_attack_animation()
		attack_timer.start()
		attack_enabled = false

func attack(target: Node) -> void:
	target.attacked(power)

func play_attack_sound() -> void:
	pass

func play_attack_animation() -> void:
	pass

func get_target() -> Node:
	return null

func _on_attack_timer_timeout() -> void:
	attack_enabled = true

func attacked(damage: float) -> void:
	change_health(-damage)

func heal(amount: float) -> void:
	$AuthorityHandler.request_authority()
	change_health(amount)

func change_health(value: float) -> void:
	health += value

func start_death() -> void:
	collision_layer = 0
	collision_mask = 0
	visible = false
	dead = true
	var death_timer: Timer = Timer.new()
	death_timer.wait_time = 5.0
	death_timer.one_shot = true
	death_timer.autostart = true
# warning-ignore:return_value_discarded
	death_timer.connect("timeout", self, "_on_deathtimer_timeout")
	add_child(death_timer)

func _on_deathtimer_timeout() -> void:
	queue_free()

func get_attack_detector() -> Node:
	return null

func serialize() -> Dictionary:
	return {
			HEALTH: health,
			POSITION: position,
			AMMO: ammo
		}

func update_state(state: Dictionary) -> void:
	if !Utils.compare_vector2(position, state[POSITION]):
		position = state[POSITION]
	if health != state[HEALTH]:
		health = state[HEALTH]
	if ammo != state[AMMO]:
		set_ammo(state[AMMO])

