extends KinematicBody2D

class_name Player

export (int) var speed = 200
var hold_position: Vector2
var velocity: Vector2 = Vector2()
var action1: bool = false
var action2: bool = false
var flip: bool = false
var initial_position: Vector2

enum {POSITION, HOLDING, HOLD_POSITION, ROTATION}

func _ready() -> void:
	hold_position = $Hand.position
	initial_position = $Hand.position
	$Label2.visible = true
	$Label2.text = String(get_path())

func _physics_process(_delta: float) -> void:
	process_movement()
	if action1:
		trigger_action()
	if action2:
		drop()

	$Sprite.flip_h = flip
	$Hand.position = hold_position

	print_target()

func serialize() -> Dictionary:
	return {
			POSITION: position,
			HOLDING: serialize_held(),
			HOLD_POSITION: hold_position,
			ROTATION: flip
		}

func serialize_held() -> Dictionary:
	if holding():
		return get_held().serialize()
	return {}

func update_state(state: Dictionary) -> void:
	position = state[POSITION]
	hold_position = state[HOLD_POSITION]
	flip = state[ROTATION]
	update_held(state[HOLDING])

func update_held(serialized_held: Dictionary):
	if !holding() && serialized_held.empty():
		return
	if serialized_held.empty():
		drop()
	elif !holding():
		var pack: SupplyPack = SupplyPackFactory.build(serialized_held)
# warning-ignore:return_value_discarded
		hold(pack)
	elif !Utils.compare_dictionaries(get_held().serialize(), serialized_held):
		drop()
		var pack: SupplyPack = SupplyPack.build(serialized_held)
# warning-ignore:return_value_discarded
		hold(pack)

func process_movement() -> void:
	if velocity == Vector2(0, 0):
		return
	if velocity.x != 0:
		flip = velocity.x < 0
		hold_position.x = initial_position.x * velocity.x

	$RayCast2D.rotation = atan2(velocity.y, velocity.x)
# warning-ignore:return_value_discarded
	move_and_slide(velocity * speed)

func trigger_action():
	if !$RayCast2D.is_colliding():
		return

	var target: Node = $RayCast2D.get_collider()
	if !holding():
		if target.has_method("interact"):
			target.interact(self)
	elif target.interact_type == InteractType.PICKUP:
		drop()
		target.interact(self)
	elif(item_can_be_used(target)):
		get_held().use(target)
	else:
		print("cant use " + get_held().get_class() + " on " + target.get_class())

func item_can_be_used(target: Node) -> bool:
	var groups: Array = target.get_groups()
	for group in groups:
		if get_held().used_on().has(group):
			return true
	return false

func print_target() -> void:
	if $RayCast2D.is_colliding():
		$Label.text = $RayCast2D.get_collider().name
	else:
		$Label.text = ""

func hold(obj: Node) -> bool:
	if !holding():
		$Hand.add_child(obj)
		return true
	return false

func drop() -> void:
	if holding():
		var obj: Node = get_held()
		$Hand.remove_child(obj)
		obj.dropped()

func holding() -> bool:
	return $Hand.get_child_count() > 0

func get_held() -> Node:
	if holding():
		return $Hand.get_child(0)
	return null

func interact_with_rigidbody(rigid_body: RigidBody2D):
	rigid_body.request_authority()
