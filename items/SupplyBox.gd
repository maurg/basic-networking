extends StaticBody2D

export(PackedScene) var supply_pack_scene
var interact_type: int = InteractType.PICKUP

func _ready():
	var supply_pack: SupplyPack = supply_pack_scene.instance()
	var sprite: Sprite = supply_pack.get_sprite()
	add_child(sprite)

func interact(player: Node) -> void:
	var supply_pack: Node = supply_pack_scene.instance()
	if !player.hold(supply_pack):
		supply_pack.queue_free()
