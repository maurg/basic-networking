extends SupplyPack

export(float) var ammo_value = 2

func _ready():
	type = "AmmoPack"

func _use(target: Minion) -> void:
	target.change_ammo(ammo_value)
	dropped()
