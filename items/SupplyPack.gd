extends Node

class_name SupplyPack

export(int) var charges = 2
enum { TYPE, CHARGE }
var type: String

func serialize() -> Dictionary:
	return {
			TYPE: type,
			CHARGE: charges
		}

func use(target: Minion) -> void:
	if charges <= 0:
		return
	charges -= 1
	_use(target)

func _use(_target: Minion) -> void:
	pass

func dropped() -> void:
	queue_free()

func used_on() -> Array:
	return get_groups()

func get_sprite() -> Sprite:
	var sprite: Sprite = $Sprite.duplicate(0)
	return sprite
