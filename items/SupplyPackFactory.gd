extends Node

class_name SupplyPackFactory

const types: Dictionary = {
	"HealPack": preload("res://items/HealPack.tscn"),
	"AmmoPack": preload("res://items/AmmoPack.tscn")
}

var type: String

static func build(attributes: Dictionary) -> SupplyPack:
	var pack: SupplyPack = types[attributes[SupplyPack.TYPE]].instance()
	pack.charges = attributes[SupplyPack.CHARGE]
	return pack
