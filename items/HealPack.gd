extends SupplyPack

export(float) var heal_value = 20.0
var frames_map: Dictionary = {
	0: 728,
	1: 729,
	2: 730
}

func _ready():
	$Sprite.frame = frames_map[charges]
	type = "HealPack"

func _use(target: Minion) -> void:
	target.heal(heal_value)
	$Sprite.frame = frames_map[charges]

func get_sprite() -> Sprite:
	var sprite: Sprite = $Sprite.duplicate(0)
	return sprite
