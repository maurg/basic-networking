This is a co-op game developed in Godot to demonstrate basic online networking.

The Game consist of a group of defenders trying to defend against a wave of attackers. The players are responsible to keep the archers with ammo and the gate keeper healthy so the attackers are kept at bay.

The Networking part consists of an implementation based on this article, https://gafferongames.com/post/networked_physics_in_virtual_reality/, by Glenn Fiedler. The idea for is to have an Authority/Ownership system that handles who has control of different nodes. It works based on the following rules:
- The Owner or Authority over a Node is responsible for updating the the node and sending updates to the network.
- Nodes with no Owner and no Authority are updated by the server to keep clients in sync.
- Ownership always has priority over Authority
- Ownership can only be obtained from Nodes with No Owner 
- Ownership can only be relinquished by the Owner
- Authority can be taken whenever
- Authority can be taken away by anyone
- Ownership and Authority are controlled by an optimist lock system where you assume authority will be given
- In case your request of Ownership/Authority is rejected the Node is snapped to the updated position of the server

The implementation of networking of this project currently:
- Has the ownership and authority systems implemented throught the use of the AuthorityHandler node in conjuction with the Orchestrator autoloaded script
- The Networking autoloaded script is responsible for the communication
- Communication uses UDP calls (rpc_unreliable) together with jitter buffers to synchronize the data received
- Server receives all communication from all clients, plays it and update all clients
- Communication delay from client to server or server to client will always be at a minimum 1 jitter buffer size, currently set at 6 frames or roughly 100ms
- Communication delay between 2 clients will always be at a minimum 2 jitter buffers size, currently set at 12 frames or 200ms
- There is no frame synchronization between clients, meaning the system does not care about how slow is your network.
- There is no attempt to reduce the amount delay between clients and server, that means a spike in communication will delay the client permanently
- There is no handling of disconnects. Easiest implementation would be to pause the game until it is back
- There is no network optimization, as in all the data is sent as is.
- There are examples on how to deal with KinematicBodies and RigidBodies
- There is no example on how to deal with Animations, my suggestion is to send which animation is being played and which frame is it at to synchronize all players
- There is no validation of current state vs arriving state. They can be checked to improve performance as less data will be set.
